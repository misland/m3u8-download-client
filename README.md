# SO WHY DO I repeat build the wheel
Its practical ffmpeg can be downloaded directly according to the m3u8 file, but some m3u8 files do not have the complete url 😑. If you want to download with ffmpeg at this time, it will be more troublesome. You need to manually edit the m3u8 file to fill in the urls of all fragments. This is ten thousand. Unacceptable, trouble! ! !

## chinese doc
[中文文档](./README_CN.md)

## prerequisite
+ have installed nodejs and nodejs version is newer than v12
+ have installed ffmpeg and add ffmpeg executor to the system path
## advantage
It only relies on nodejs at runtime, without any third-party libraries, and no node_modules black hole😃

## usage
1. find your m3u8 file in browser,as follow:
![sample](./samples/mouxun.jpg)
2. then right click the request,copy the full url of m3u8 file
3. import index.js file
4. build a client instance，call download function to download

there will be a mp4 file in the root directory of your code when download successfully
The constructor currently receives five parameters, the specific definitions are as follows:

file：the url of the m3u8 file found by step 1
hasPrefix：Some m3u8 file urls only have .ts file name without the prefix url. This parameter is used to set a flag,set true if .ts file has full url,if not,set false
hasParam：the url in some m3u8 urls contains parameters. Setting these parameters to true if the url has parameters,if not,set false
deleteTemFile：whether delete the cache file when download completely
output：the final output mp4 file name, such as save as tem.mp4

## examples
1.when .ts file name in m3u8 file only has file name
```
let target='https://apd-a3f81ff63432030f46a06614fdb6b814.v.smtcdns.com/omts.tc.qq.com/ANlUPJ9H51t0nqmjZjpT-is1DXJHilkCc6DEugGCyyNI/uwMROfz2r57IIaQXGdGnC2dXOm7hjGQsChse-qVxwu7rgi-F/svp_50069/Wv-CUToQnoWt0hIzlZ9ZRVkyKG2MHp9auhZXF7dwf89vvzaMnEt8Ev4ZnZydIEymuFbFc396gsg6dJlxe5SlMkBXS4qB5EQoMKoygN_kIp17tT1v72K91Id3YnpoxLoKHoiZhlGcWF04DcGiAxj8j1AIWfMMvFUKyinzrfB4DvXsbIfS9maQ-A/gzc_1000035_0b534uariaabpqaai4gmrzqzjzodctsqcfca.f304110.ts.m3u8?ver=4&v_idx=0&ctime=2022-01-05 11:40:15';
  let client = new download(
    target,
    false,
    true,
    false,
    'test.mp4');
  await client.download();
```
2.when .ts file name in m3u8 file has the full url<br>
```
let target = 'https://valipl.cp31.ott.cibntv.net/69766128FD93F71AF54BA3347/03000900005FBB675CDDF956ECC37EBCA100AC-5E0D-4973-A5EA-696D9B9DB386.m3u8?ccode=0502&duration=31&expire=18000&psid=88ccb5b8fd2de8b9f187eacfb374907a43346&ups_client_netip=3af612e2&ups_ts=1641350726&ups_userid=&utid=tQzYGVKoyH8CATr2EuJJc5vH&vid=XNDk3NjUyMTg1Mg&vkey=B2906ab85b5ccebc12382768363cea216&sm=1&operate_type=1&dre=u37&si=73&eo=0&dst=1&iv=0&s=&type=mp4hd3v3&bc=2&t=f30015f5910a099&cug=1&rid=20000000CA6E79B664AED5FF7EC16111A5CD585B02000000';
let client = new download(
  target,
  true,
  true,
  false,
  'test.mp4');
await client.download();
```

# Disclaimer
This code is only used for testing and sharing. Users should pay attention to the copyright issues of the video provider. If the use of this library for illegal operations infringes the rights of others, the user is fully responsible, and I am not responsible.
